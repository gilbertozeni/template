FROM openjdk:15-jdk-alpine
EXPOSE 8080
ADD /build/libs/template*.jar /opt/api.jar
ENTRYPOINT exec java $JAVA_OPTS -jar /opt/api.jar