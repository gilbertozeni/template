# template
Template project with basic configuration

## Requirements
* [JDK 15](https://openjdk.java.net/projects/jdk/15/)
* [Gradle](https://gradle.org/)

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Future improvements
* Add [BlockHound](https://github.com/reactor/BlockHound) to tests