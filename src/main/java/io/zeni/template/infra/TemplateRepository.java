package io.zeni.template.infra;

import io.zeni.template.business.port.RepositoryPort;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class TemplateRepository implements RepositoryPort {
    @Override
    public Mono<String> findTemplate() {
        return Mono.just("Hello, world!");
    }
}
