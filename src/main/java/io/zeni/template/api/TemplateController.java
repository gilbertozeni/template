package io.zeni.template.api;

import io.zeni.template.api.dto.TemplateDto;
import io.zeni.template.business.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/template")
public class TemplateController {

    private TemplateService service;

    @Autowired
    public TemplateController(TemplateService service) {
        this.service = service;
    }

    @GetMapping
    public Mono<TemplateDto> findTemplate() {
        return this.service.find()
                .flatMap(s -> Mono.just(new TemplateDto(s)));
    }

}
