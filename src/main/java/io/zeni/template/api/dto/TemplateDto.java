package io.zeni.template.api.dto;

public class TemplateDto {
    private String message;

    public TemplateDto() {
    }

    public TemplateDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
