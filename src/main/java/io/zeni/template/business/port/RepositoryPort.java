package io.zeni.template.business.port;

import reactor.core.publisher.Mono;

public interface RepositoryPort {
    Mono<String> findTemplate();
}
