package io.zeni.template.business;

import io.zeni.template.business.port.RepositoryPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class TemplateService {

    private RepositoryPort repository;

    @Autowired
    public TemplateService(RepositoryPort repository) {
        this.repository = repository;
    }

    public Mono<String> find() {
        return this.repository.findTemplate();
    }
}
