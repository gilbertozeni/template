package io.zeni.template.config;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MonitoringConfig {
    private static final String VERSION_TAG = "version";
    private static final String SERVICE_TAG = "service";

    @Bean
    MeterRegistryCustomizer<MeterRegistry> customizer(BuildProperties buildInfo) {
        return registry -> registry
                .config()
                .commonTags(
                        SERVICE_TAG, buildInfo.getName(),
                        VERSION_TAG, buildInfo.getVersion());
    }
}
