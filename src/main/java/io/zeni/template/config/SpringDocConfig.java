package io.zeni.template.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDocConfig {
    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .packagesToScan("io.zeni.template.api")
                .group("template")
                .pathsToMatch("/template/**")
                .build();
    }

    @Bean
    public OpenAPI springOpenAPI(BuildProperties buildInfo) {
        return new OpenAPI()
                .info(new Info().title("template")
                        .description("Template project with basic configuration")
                        .version(buildInfo.getVersion()));
    }
}
